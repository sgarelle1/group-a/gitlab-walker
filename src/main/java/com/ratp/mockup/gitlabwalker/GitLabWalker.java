package com.ratp.mockup.gitlabwalker;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ratp.mockup.gitlabwalker.model.SimpleProject;
import com.ratp.mockup.gitlabwalker.model.SubGroup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

public class GitLabWalker {
    private final ObjectMapper objectMapper;

    public GitLabWalker() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static void main(String[] args) {
        try {
            GitLabWalker gitLabWalker = new GitLabWalker();
            Map<String, Map<String, String>> fullMap = new HashMap<>();
            Set<String> languages = new HashSet<>();
            List<SubGroup> subGroups = gitLabWalker.getSubGroups();
            System.out.println("Query GitLab...");
            for (SubGroup subGroup : subGroups) {
                System.out.println(subGroup.getName());
                List<SimpleProject> simpleProjects = gitLabWalker.getSimpleProjects(subGroup.getId());
                for (SimpleProject simpleProject : simpleProjects) {
                    Map<String, String> languagesStats = gitLabWalker.getLanguages(simpleProject.getId());
                    languages.addAll(languagesStats.keySet());
                    fullMap.put(subGroup.getName() + ':' + simpleProject.getName(), languagesStats);
                }
            }

            System.out.println("Sort it out in CSV...");
            // CSV Header
            System.out.print("project;");
            for (String language : languages) {
                System.out.print(language + ';');
            }
            System.out.println();

            // CSV Lines
            for (Map.Entry<String, Map<String, String>> project : fullMap.entrySet()) {
                System.out.print(project.getKey() + ';');
                for (String language : languages) {
                    String percentage = project.getValue().get(language);
                    System.out.print((percentage == null ? ';' : percentage + ';'));
                }
                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<SubGroup> getSubGroups() throws Exception {
        URL url = new URL("https://gitlab.com/api/v4/groups/6397356/subgroups?per_page=100");
        String response = httpGet(url);
        return objectMapper.readValue(response, new TypeReference<>() {
        });
    }

    public String httpGet(String url) throws Exception {
        return httpGet(new URL(url));
    }

    private List<SimpleProject> getSimpleProjects(String subGroupId) throws Exception {
        URL url = new URL("https://gitlab.com/api/v4/groups/" + subGroupId + "/projects?simple=true");
        String response = httpGet(url);
        return objectMapper.readValue(response, new TypeReference<>() {
        });
    }

    private Map<String, String> getLanguages(String projectId) throws Exception {
        Map<String, String> languages = new HashMap<>();
        URL url = new URL("https://gitlab.com/api/v4/projects/" + projectId + "/languages");
        String response = httpGet(url);
        JsonNode jsonNode = objectMapper.readTree(response);
        Iterator<String> fieldsIterator = jsonNode.fieldNames();
        while (fieldsIterator.hasNext()) {
            String language = fieldsIterator.next();
            languages.put(language, jsonNode.get(language).asText());
        }
        return languages;
    }

    private String httpGet(URL url) throws IOException {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestProperty("PRIVATE-TOKEN", "KgArJW6zbLFT3wwdAo9z");
        con.setRequestProperty("sudo", "9802431");
        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        return content.toString();
    }
}
