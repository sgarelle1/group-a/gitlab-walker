package com.ratp.mockup.gitlabwalker.model;

public class SimpleProject {
    private String id;
    private String description;
    private String name;
    private String name_with_namespace;
    private String path;

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getName_with_namespace() {
        return name_with_namespace;
    }

    public String getPath() {
        return path;
    }
}
