package com.ratp.mockup.gitlabwalker.model;

public class SubGroup {
    private String id;
    private String web_url;
    private String name;
    private String path;

    public String getId() {
        return id;
    }

    public String getWeb_url() {
        return web_url;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }
}
