package com.ratp.mockup.gitlabwalker;

public class GitLabQuery {
    public static void main(String[] args) throws Exception {
        GitLabWalker gitLabWalker = new GitLabWalker();
        String projectStats = gitLabWalker.httpGet("https://gitlab.com/api/v4/audit_events");
        System.out.println("projectStats = " + projectStats);
    }
}
