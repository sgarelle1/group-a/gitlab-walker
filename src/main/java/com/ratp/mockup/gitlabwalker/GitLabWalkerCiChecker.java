package com.ratp.mockup.gitlabwalker;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ratp.mockup.gitlabwalker.model.SimpleProject;
import com.ratp.mockup.gitlabwalker.model.SubGroup;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

public class GitLabWalkerCiChecker {
    private final ObjectMapper objectMapper;

    public GitLabWalkerCiChecker() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static void main(String[] args) {
        try {
            GitLabWalkerCiChecker gitLabWalker = new GitLabWalkerCiChecker();
            List<SubGroup> subGroups = gitLabWalker.getSubGroups();
            for (SubGroup subGroup : subGroups) {
                List<SimpleProject> simpleProjects = gitLabWalker.getSimpleProjects(subGroup.getId());
                for (SimpleProject simpleProject : simpleProjects) {
                    boolean gitLab = gitLabWalker.hasGitLabCiDescriptor(simpleProject.getId());
                    boolean jenkins = gitLabWalker.hasJenkinsCiDescriptor(simpleProject.getId());
                    System.out.println(subGroup.getName() + ";" + simpleProject.getName() + ";" + (gitLab ? "GitLabCi" : (jenkins ? "JenkinsCi" : "None")));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean hasJenkinsCiDescriptor(String projectId) {
        try {
            httpGet("https://gitlab.com/api/v4/projects/" + projectId + "/repository/files/Jenkinsfile?ref=develop");
        } catch (FileNotFoundException ex) {
            try {
                httpGet("https://gitlab.com/api/v4/projects/" + projectId + "/repository/files/jenkins%2FJenkinsfile?ref=develop");
                return true;
            } catch (FileNotFoundException ex1) {
                return false;
            } catch (Exception e) {
                System.err.println("Failed to retrieve GitLabCi descriptor");
                return false;
            }
        } catch (Exception e) {
            System.err.println("Failed to retrieve GitLabCi descriptor");
            return false;
        }
        return true;
    }

    private boolean hasGitLabCiDescriptor(String projectId) {
        try {
            httpGet("https://gitlab.com/api/v4/projects/" + projectId + "/repository/files/.gitlab-ci.yml?ref=develop");
        } catch (FileNotFoundException ex) {
            return false;
        } catch (Exception e) {
            System.err.println("Failed to retrieve GitLabCi descriptor");
            return false;
        }
        return true;
    }

    private List<SubGroup> getSubGroups() throws Exception {
        URL url = new URL("https://gitlab.com/api/v4/groups/6397356/subgroups?per_page=100");
        String response = httpGet(url);
        return objectMapper.readValue(response, new TypeReference<>() {
        });
    }

    public String httpGet(String url) throws Exception {
        return httpGet(new URL(url));
    }

    private List<SimpleProject> getSimpleProjects(String subGroupId) throws Exception {
        URL url = new URL("https://gitlab.com/api/v4/groups/" + subGroupId + "/projects?simple=true");
        String response = httpGet(url);
        return objectMapper.readValue(response, new TypeReference<>() {
        });
    }


    private String httpGet(URL url) throws IOException {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestProperty("PRIVATE-TOKEN", "KgArJW6zbLFT3wwdAo9z");
        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        return content.toString();
    }
}
